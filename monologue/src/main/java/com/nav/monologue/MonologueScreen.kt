package com.nav.monologue

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.nav.coreui.DIScope
import com.nav.coreui.get
import com.nav.monologue.destinations.MonologueScreenDestination
import com.ramcosta.composedestinations.annotation.Destination

data class MonologueScreenNavArgs(
    val text: String
)

interface MonologueNavigator {
    fun navigateUp()
    fun navigateForward()
}

@Destination(navArgsDelegate = MonologueScreenNavArgs::class)
@Composable
fun MonologueScreen(
    navigator: MonologueNavigator,
    navArgs: MonologueScreenNavArgs,
) {
    DIScope(MonologueScreenDestination) {
        val data:SomeDataClass = get()

        BackHandler {
            navigator.navigateUp()
        }

        Surface {
            Scaffold {
                Column(
                    Modifier
                        .padding(it)
                        .fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = navArgs.text,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp),
                        textAlign = TextAlign.Center
                    )
                    Button(onClick = {
                        navigator.navigateForward()
                    }) {
                        Text(text = data.text)
                    }
                }
            }
        }
    }
}