package com.nav.greeting

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.nav.coreui.DIScope
import com.nav.greeting.destinations.GreetingScreenDestination
import com.ramcosta.composedestinations.annotation.Destination

interface GreetingNavigator {
    fun openMonologue(text:String)
}

@Destination
@Composable
fun GreetingScreen(navigator: GreetingNavigator) {
    DIScope(scope = GreetingScreenDestination) {
        Surface {
            Scaffold {
                Column(
                    Modifier
                        .padding(it)
                        .fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(text = "hello")
                    Button(onClick = {
                        navigator.openMonologue("some text for next screen")
                    }) {
                        Text(text = "another")
                    }
                }
            }
        }
    }
}