package com.nav.demo.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.nav.demo.theme.theme.DemoNavigationTheme
import com.nav.demo.util.AppNavigation

class MainActivity : ComponentActivity() {

    @OptIn(ExperimentalAnimationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DemoNavigationTheme {
                val navController = rememberAnimatedNavController()
                AppNavigation(navController = navController)
            }
        }
    }
}