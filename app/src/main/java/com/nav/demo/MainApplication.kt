package com.nav.demo

import android.app.Application
import com.nav.demo.di.AppModule
import com.nav.demo.di.GreetingsModule
import com.nav.demo.di.MonologueModule
import org.koin.core.context.GlobalContext.startKoin

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(
                modules = listOf(
                    AppModule.module,
                    MonologueModule.module,
                    GreetingsModule.module
                )
            )
        }
    }
}