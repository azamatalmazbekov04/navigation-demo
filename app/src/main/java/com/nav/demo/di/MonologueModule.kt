package com.nav.demo.di

import com.nav.monologue.SomeDataClass
import com.nav.monologue.destinations.MonologueScreenDestination
import org.koin.dsl.module

object MonologueModule {
    val module = module {
        scope<MonologueScreenDestination> {
            scoped { SomeDataClass() }
        }
    }
}