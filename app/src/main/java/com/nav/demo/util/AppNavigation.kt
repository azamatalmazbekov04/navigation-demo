package com.nav.demo.util

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavDestination
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavHostController
import com.nav.greeting.destinations.GreetingScreenDestination
import com.nav.monologue.destinations.MonologueScreenDestination
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.dynamic.routedIn
import com.ramcosta.composedestinations.navigation.dependency
import com.ramcosta.composedestinations.spec.DestinationSpec
import com.ramcosta.composedestinations.spec.NavGraphSpec
import com.ramcosta.composedestinations.spec.Route

object NavGraphs {

    val greeting = object : NavGraphSpec {
        override val route: String = "greeting_nav_graph"
        override val startRoute: Route = GreetingScreenDestination routedIn this
        override val destinationsByRoute = listOf<DestinationSpec<*>>(
            GreetingScreenDestination,
            MonologueScreenDestination
        ).routedIn(this).associateBy { it.route }
    }

    val root = object : NavGraphSpec {
        override val route: String = "root_nav_graph"
        override val startRoute: Route = greeting
        override val destinationsByRoute = emptyMap<String, DestinationSpec<*>>()
        override val nestedNavGraphs: List<NavGraphSpec> = listOf(greeting)
    }
}


fun NavDestination.navGraph(): NavGraphSpec {
    hierarchy.forEach { destination ->
        NavGraphs.root.nestedNavGraphs.forEach { navGraph ->
            if (destination.route == navGraph.route) {
                return navGraph
            }
        }
    }
    throw RuntimeException("Unknown nav graph for destination $route")
}

@Composable
internal fun AppNavigation(
    navController: NavHostController,
    modifier: Modifier = Modifier,
) {
    DestinationsNavHost(navController = navController,
        navGraph = NavGraphs.greeting,
        modifier = modifier,
        dependenciesContainerBuilder = {
            dependency(
                CommonNavGraphNavigator(
                    navBackStackEntry.destination.navGraph(),
                    navController,
                )
            )
        })
}
