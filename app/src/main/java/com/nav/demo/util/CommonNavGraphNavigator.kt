package com.nav.demo.util

import androidx.navigation.NavController
import com.nav.greeting.GreetingNavigator
import com.nav.monologue.MonologueNavigator
import com.nav.monologue.destinations.MonologueScreenDestination
import com.ramcosta.composedestinations.dynamic.within
import com.ramcosta.composedestinations.navigation.navigateTo
import com.ramcosta.composedestinations.spec.NavGraphSpec

class CommonNavGraphNavigator(
    private val navGraph: NavGraphSpec,
    private val navController: NavController,
) : GreetingNavigator,
    MonologueNavigator {

    override fun navigateUp() {
        navController.navigateUp()
    }

    override fun openMonologue(text: String) {
        navController.navigate((MonologueScreenDestination(text = text) within navGraph).route)
    }

    override fun navigateForward() {
        navController.navigate((MonologueScreenDestination(text = "second monologue screen") within navGraph).route)
    }
}