package com.nav.coreui

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.lifecycle.Lifecycle
import com.nav.greeting.ComposableLifecycle
import org.koin.core.Koin
import org.koin.core.annotation.KoinInternalApi
import org.koin.core.context.GlobalContext
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.core.qualifier.TypeQualifier
import org.koin.core.scope.Scope
import org.koin.ext.getFullName
import org.koin.java.KoinJavaComponent.getKoin
import java.util.UUID
import org.koin.androidx.compose.get as koinGet

internal fun getGlobalKoin(): Koin = GlobalContext.get()

@OptIn(KoinInternalApi::class)
@PublishedApi
internal val LocalScope = compositionLocalOf {
    getGlobalKoin().scopeRegistry.rootScope
}

internal fun <T : Any> T.getScopeId(uuid:String) =
    this::class.getFullName() + "@" + System.identityHashCode(this)+uuid

internal fun <T : Any> T.getScopeName() = TypeQualifier(this::class)
fun <T : Any> T.newScope(uuid:String,source: Any? = null): Scope {
    return getKoin().getOrCreateScope(getScopeId(uuid), getScopeName(), source)
}

@Composable
inline fun <reified T> get(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null,
): T {
    val scope = LocalScope.current
    return remember(qualifier, parameters) {
        scope.get(qualifier, parameters)
    }
}

@Composable
inline fun <reified T : Any> DIScope(
    scope: T,
    crossinline content: @Composable () -> Unit
) {

    val uniqueId:String = rememberSaveable {
        UUID.randomUUID().toString()
    }
    val createdOrFetchedScope:Scope by lazy {
        scope.newScope(uniqueId)
    }
    val currentScope = LocalScope.current

    var providedScope:Scope = createdOrFetchedScope.also {
        it.linkTo(currentScope)
    }

    ComposableLifecycle { _, event ->
        when (event) {
            Lifecycle.Event.ON_DESTROY -> {
                providedScope.close()
            }
            else -> Unit
        }
    }

    BackHandler {
        providedScope.close()
    }

    CompositionLocalProvider(LocalScope provides providedScope) {
        content()
    }
}
