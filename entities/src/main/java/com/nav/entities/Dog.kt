package com.nav.entities

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Dog(
    val name:String,
    val age:Int
): Parcelable